var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var expect = chai.expect;
var WebSocket = require('ws');


var socketURL = 'http://localhost:5001';

var options = {
    transports: ['websocket'],
    'force new connection': true
};


describe('WFT', function () {
    it('test should be equal test', function () {
        'test'.should.equal('test');
    });

    it('Connect user', function (done) {

        var client1 = new WebSocket(socketURL);

        expect(client1).to.be.a('Object');

        client1.on('message', function (data) {
            data.should.be.a('string');
            data.should.equal('connected');
            client1.close();
            done()
        });

    });

    describe('Messaging', function () {
        it('Connect 2 users', async function () {
            var client1 = await new WebSocket(socketURL);

            client1.send('test 1');

            var client2 = await new WebSocket(socketURL);

            client2.send('test 2');
        });
    });


});
