FROM node:8

RUN apt-get update && apt-get install -y vim wget dialog net-tools

RUN mkdir -p /app

ADD . ./app

WORKDIR /app

RUN ls

RUN npm -v

RUN node -v

RUN npm i

RUN npm i -g nodemon typescript ts-node

#RUN tsc

EXPOSE 5001

CMD ["npm", "start"]