
interface ShipInterface {
    position: any[] ;
    length: number;
    damanged: number;
}

export default class Ship implements ShipInterface {
    position: any[] = [];
    length: number;
    damanged: number = 0;
    alive: boolean = false;

    constructor(length: number, position: any = []) {
        this.length = length;
        this.position = position;
    }

    isAlive() {
        if (this.alive === false) return false;
        if (this.damanged < this.length) {
            this.alive = false;
        }
        return true;
    }
}