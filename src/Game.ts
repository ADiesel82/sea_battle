import Ship from "./Ship";
import GameSettings from "./GameSettings";
import Client from "./Client";
import GameServer from "./GameServer";


export default class Game {
    private id: number;
    private gameServer: GameServer;
    private turn: number; //clientId
    private clientIds: any = {};
    private clients: Client[] = [];
    private ships: Ship[] = [];
    private maps: any[] = [];
    private settings: any;
    private readyCount: number = 0;
    private mapSize: number;
    private wonWhenSuccessShotsCount: number = 0;
    private successShots: any = [];

    constructor(gameServer: GameServer, id: number) {
        this.id = id;
        this.gameServer = gameServer;
        this.settings = GameSettings.sizes[gameServer.settings.mapSize];
        this.mapSize = gameServer.settings.mapSize;
        for (let i in this.settings.ships) {
            this.wonWhenSuccessShotsCount = this.wonWhenSuccessShotsCount + this.settings.ships[i] * i;
        }
    }

    private broadcast(command: string, data: any): void {
        if (this.clients.length > 0) {
            this.clients.forEach(function each(client: Client) {
                if (client.isActive()) {
                    client.sendTextResponse(command, data);
                }
            });
        }
    }

    addClient(client: Client) {
        this.clients.push(client);
        this.maps[client.id] = this.addMap();
        this.clientIds[this.clients.indexOf(client)] = client.id;
        this.successShots[client.id] = 0;

        client.sendTextResponse("init", {
            "cliendId": client.id,
            "gameId": this.id,
            "map": this.maps[client.id],
            "settings": this.settings
        });
    }

    private addMap(): any {
        const map = [];
        for (let i = 0; i < this.mapSize; i++) {
            const row = [];
            for (let j = 0; j < this.mapSize; j++) {
                row.push("");
            }
            map.push(row);
        }

        return map;
    }

    callFn(methodName: string, data: any): void {
        if (this[methodName]) {
            if (data) {
                return this[methodName].apply(this, Object.values(data));
                // return this[methodName]..keys(myObject).map((this, data);
            } else {
                return this[methodName]();
            }
        } else {
            console.log("Game has no method: ", methodName);
        }
    }


    private sendShot(shot: any, clientId: number): void {
        let nextTurn;
        let success = false;
        let won = false;
        if (this.turn == clientId) {
            const opponentId = this.getNextTurnClientId(clientId);
            if (this.maps[opponentId][shot.x] && this.maps[opponentId][shot.x][shot.y]) {
                if (this.maps[opponentId][shot.x][shot.y] !== "") { //has a ship
                    nextTurn = clientId;
                    success = true;
                    this.successShots[clientId]++;
                    // console.log(this.successShots[clientId], '===', this.wonWhenSuccessShotsCount);
                    if (this.successShots[clientId] === this.wonWhenSuccessShotsCount) won = true;
                }
            } else {
                nextTurn = opponentId;
                if (nextTurn === clientId) throw Error("Error in logic of getNextTurnClientId");
            }
        } else {
            nextTurn = this.turn;
        }
        this.turn = nextTurn;
        this.broadcast("move", {"turn": nextTurn, "shot": {"x": shot.x, "y": shot.y, "author": clientId}, "success": success, "won": won});
    }

    private getNextTurnClientId(clientId?: number = -1): any {
        let clientIds = Object.values(this.clientIds);

        if (clientId  > 0) {
            if (clientIds[0] === clientId) {
                clientIds.reverse();
            }
        }
        return clientIds[0];
    }

    private setShips(ships: any, clientId: number): void {
        for (let length in ships) {
            for (let i in ships[length]) {
                const ship = ships[length][i];
                this.ships[clientId] = new Ship(<Number>length, ship);
                for (let k in ship) {
                    let x = ship[k][0],
                        y = ship[k][1];
                    this.maps[clientId][x][y] = length;
                }
            }
        }
        this.readyCount++;
        this.checkAllReady();
    }

    private checkAllReady() {
        if (this.readyCount === 2) {
            const nextTurn = this.getNextTurnClientId();
            this.turn = nextTurn;
            // console.log(this.maps);
            this.broadcast("ready", {"turn": nextTurn, "maps": this.maps});
        }
    }

    onClientDisconnect(client: Client) {
        this.broadcast("stop", "Your opponent was disconnected");
        this.clients.splice(this.clients.indexOf(client), 1);

        this.gameServer.onClientDisconnect(this);
    }
}