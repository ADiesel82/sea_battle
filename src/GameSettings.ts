export default class GameSettings {
    static sizes: any = {
        6: {
            ships: {
                /** legth: count */
                4: 1,
                3: 0,
                2: 2,
                1: 2
            }
        },
        5: {
            ships: {
                /** legth: count */
                4: 0,
                3: 1,
                2: 1,
                1: 1
            }
        }
    };
}