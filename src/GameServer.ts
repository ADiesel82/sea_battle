import *  as WebSocket from "ws";
import Client from "./Client";
import Game from "./Game";

export default class GameServer {

    settings: { mapSize: number } = {
        mapSize: 6
    };

    protected isAlive: boolean = false;
    private games: Game[] = [];
    private clients: any[] = [];
    private wss: WebSocket.Server;

    initWebSocket(options?: WebSocket.ServerOptions): void {

        this.wss = new WebSocket.Server(options);

        this.wss.on("listening", () => {
            console.log("Starting websocket on port: " + this.wss.options.port);
        });

        this.wss.on("connection", (ws: WebSocket) => {
            const cliendId = Date.now() + Math.ceil(Math.random());
            const game = this.getGameInstance();
            const client = new Client(cliendId, game, ws);
            game.addClient(client);
            this.clients.push(client);


            ws.isAlive = true;

            ws.on("pong", () => {
                ws.isAlive = true;
            });
        });

        const interval = setInterval(() => {
            this.wss.clients.forEach(function each(ws : WebSocket) {
                if (ws.isAlive === false) return ws.terminate();
                ws.isAlive = false;
                ws.ping("", false, true);
            });
        }, 30000);
    }


    private getGameInstance(): any {
        const lastIndex = this.games.length;
        if (lastIndex > 0 && this.games[lastIndex-1] && this.games[lastIndex-1].clients.length < 2) {
            return this.games[lastIndex-1];
        }
        this.games.push(new Game(this, lastIndex));
        return this.games[lastIndex];
    }

    onClientDisconnect(game: Game): void {
        if (game.clients.length == 0) {
            this.games.splice(this.games.indexOf(game), 1);
        }
    }
}