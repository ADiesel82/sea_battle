import GameServer from "./GameServer";
import Game from "./Game";
import * as WebSocket from "ws";
import * as util from "util";


export default class Client {

    id: number;
    private ws: WebSocket;
    private game: Game;
    map: any = [];

    constructor(id: number, game: Game, ws: WebSocket) {
        this.id = id;
        this.game = game;
        this.ws = ws;

        this.setupMessages();
    }
    setupMessages() {
        this.ws.on("message", (message: string) => {
            this.handleTextRequest(message);
        });
        this.ws.on("close", (reasonCode: number, description: string) => {
            console.log("Close " + (new Date()) + " Peer " + description + " disconnected.");
            this.game.onClientDisconnect(this);
        });
        this.ws.on("error", (reasonCode: number, description: string) => {
            console.log("Error " + (new Date()) + " Peer " + description + " disconnected.");
            // this.onDisconnect();
            // this.game.onClientDisconnect(this);
        });
    }


    private handleTextRequest(message: string): void {
        console.log("received: %s", message);
        let request = undefined;
        try {
            request = JSON.parse(message);
        } catch (e) {
            console.log(e);
        }

        if (request) {
            try {
                // this.sendTextResponse(request.command,
                    this.game.callFn(request.command, request.data);
            // );
            } catch (e) {
                console.log(e);
                this.sendTextResponse("error", util.format("Error raised during command %s: %s", request.command, e));
            }
        } else {
            this.sendTextResponse("error", "Undefined command: " + message);
        }
    }

    sendTextResponse(command: string, data?: any): void {
        const response: any = {
            command: command,
            data: data
        };
        this.ws.send(JSON.stringify(response));
    }

    private onDisconnect(): void {

    }

    isActive(): boolean {
        return (this.ws.readyState === WebSocket.OPEN);
    }


}
