import * as express from "express";
import GameServer from "./src/GameServer";

const server = new GameServer();
server.initWebSocket({ port: 5001 });

const app = express();
app.use(express.static("html"));

app.listen(3000, function () {
    console.log("The game you can find here http://localhost:3000");
    console.log("Single player game you can find here http://localhost:3000/player.html ");
});


